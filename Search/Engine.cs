﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using FastFuzzyStringMatcher;
using TwinFinder.Matching.StringFuzzyCompare.Common;
using TwinFinder.Matching.StringFuzzyCompare.StringCostFunctions;

namespace Search
{
    public class Engine
    {
        readonly List<string> _data;
        readonly StringMatcher<string> _index = new StringMatcher<string>(MatchingOption.RemoveSpacingAndLinebreaks);
        
        // move all code that should be run once in constructor to speed-up tests (all initializations)
        public Engine()
        {
            _data = System.IO.File.ReadAllLines("../../../data.dat").ToList();
            InitIndex();
        }

        void InitIndex()
        {
            foreach (var e in _data)
            {
                _index.Add(e, "");
            }
        }
        
        // just an example
        //string Fuzzy(string input)
        //{
        //    var d = _index.Search(input, 80.0f);

        //    if (!d.Any())
        //        return string.Empty;

        //    d.SortByClosestMatch();

        //    return d.First().Keyword;
        //}


        string Fuzzy(string input)
        {
            if(input == null || input.Equals(String.Empty) || input.Equals(" "))
                return String.Empty;

            input = input.Replace("_", " ");

            DamerauLevenshteinDistance dld = new DamerauLevenshteinDistance();
            dld.CaseSensitiv = false;


            /* 86.88977; score 172592 ; ngram length  = 2;   similarity > 0.499999f */
            /* 88.70125; score 176834 ; ngram length  = 2;   similarity > 0.499999f; contains input */
            NGramDistance ngd = new NGramDistance();
            ngd.NGramLength = 2;
            ngd.CaseSensitiv = false;

            var damRes = new List<KeyValuePair<float, string>>();

            foreach (var targetSentence in _data)
            {
                var semilarity = ngd.Compare(targetSentence, input);
                damRes.Add(new KeyValuePair<float, string>(semilarity, targetSentence));
            }

            var searchRes = damRes.OrderByDescending(v => v.Key).ToList();

            if (searchRes.Any())
            {
                if (searchRes.First().Key > 0.499998f)
                {
                    //if (searchRes.First().Key < 0.66)
                    //{
                    //    NGramDistance ngdForLow = new NGramDistance();
                    //    ngdForLow.NGramLength = 15;
                    //    ngdForLow.CaseSensitiv = false;

                    //    var sem = ngdForLow.Compare(input, searchRes.First().Value);
                    //    if (sem < 0.5f)
                    //        return string.Empty;

                    //}

                    if (searchRes.First().Key < 0.6f)
                    {
                        int exist = 0;
                        var splittedInput = input.ToLowerInvariant().Split(new char[] {' ', '-'}).ToList();
                        var splittedPossibleVal = searchRes.First().Value.ToLowerInvariant().Split(new char[] {' ', '-'});

                        //splittedInput = splittedInput.Where(v => v != "").ToList();
                        //splittedPossibleVal = splittedPossibleVal.Where(v => v != "").ToArray();

                        var intersec = splittedPossibleVal.Intersect(splittedInput);

                        var norm = (float)intersec.Count() / (float)splittedPossibleVal.Length;
                        if(norm <= 0.609999f)  // 0.539999f
                            return String.Empty;

                    }

                    return searchRes.First().Value;
                }

                if(input.Contains(searchRes.First().Value))
                    return searchRes.First().Value;

                var reversed = input.Split(new char[] {' '}).Reverse().Aggregate((p, c) => $"{p} {c}");
                if (reversed.Contains(searchRes.First().Value))
                    return searchRes.First().Value;

                /*stop words*/

                if (input.Replace("-", " ").Contains(searchRes.First().Value.Replace("-", " ")))
                    return searchRes.First().Value;

                var reversedReplaced = input.Split(new char[] { ' ' }).Reverse().Aggregate((p, c) => $"{p} {c}");
                if (reversedReplaced.Replace("-", " ").Contains(searchRes.First().Value.Replace("-", " ")))
                    return searchRes.First().Value;

            }

            var tranlatedToRussian = string.Empty;
            foreach (char inputChar in input)
            {
                NltkUtils.Mapping.TryGetValue(inputChar.ToString(), out var val);
                tranlatedToRussian += val;
            }

            var translatedToEnglish = string.Empty;
            foreach (char inputChar in input)
            {
                var mapped = NltkUtils.Mapping.FirstOrDefault(kvp => kvp.Value.Equals(inputChar.ToString()));
                translatedToEnglish += mapped.Key;
            }
            var a = 5;

            if (tranlatedToRussian.Length >= translatedToEnglish.Length)
            {
                var damResTranslated = new List<KeyValuePair<float, string>>();

                foreach (var targetSentence in _data)
                {
                    var semilarity = ngd.Compare(targetSentence, tranlatedToRussian);
                    damResTranslated.Add(new KeyValuePair<float, string>(semilarity, targetSentence));
                }

                var final = damResTranslated.OrderByDescending(v => v.Key).ToList();

                if (final.First().Key > 0.499998f)
                    return final.First().Value;

            }
            else
            {
                var damResTranslated = new List<KeyValuePair<float, string>>();

                foreach (var targetSentence in _data)
                {
                    var semilarity = ngd.Compare(targetSentence, translatedToEnglish);
                    damResTranslated.Add(new KeyValuePair<float, string>(semilarity, targetSentence));
                }

                var final = damResTranslated.OrderByDescending(v => v.Key).ToList();

                if (final.First().Key > 0.499998f)
                    return final.First().Value;
            }

            //var dlSemi = dld.Compare(input, searchRes.First().Value);

            var replaceWhitespaceToOne = Regex.Replace(input.Replace("-", " "), @"\s+", " ");
            var replaceWhitespaceToOnePossible = Regex.Replace(searchRes.First().Value.Replace("-", " "), @"\s+", " ");

            {
                var splittedInput = replaceWhitespaceToOne.ToLowerInvariant().Split(new char[] { ' '});
                var splittedPossibleVal = replaceWhitespaceToOnePossible.ToLowerInvariant().Split(new char[] { ' ' });

                var intersec = splittedInput.Intersect(splittedPossibleVal);

                var norm = (float)intersec.Count() / (float)splittedPossibleVal.Length;
                if (norm <= 0.7)  // 0.539999f
                    return String.Empty;

                return searchRes.First().Value;
            }


            return string.Empty;
        }



        // Entry Point for Test engine - DON`T change specification (this method is called for each test-case)
        public string Find(string input)
        {
           // TODO: implement your logic
           // Happy coding !!!
           return Fuzzy(input);
        }
    }

}
